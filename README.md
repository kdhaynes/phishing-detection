# Combining Adversarial Synthesized Data and Deep Neural Networks to Improve Phishing Detection

This project extends the experiments in [1] by using an artificial neural network (ANN).  In addition, I used BERT and ELECTRA to detect phishing URLs and compared all of my results against recent literature.

I performed all of my work in seven notebooks:
- PhishNN.ipynb: Uses ANNs to predict phishing websites from features, performing a model search to find the optimal architecture and evaluating four different training and testing dataset combinations.

- PhishBERT1_PredictWiki.ipynb: Uses pre-trained BERT-Base with fine-tuining to predict phishing from URLs.
- PhishBERT2_PreTrainURL.ipynb: Pre-trains BERT using a URL-specific vocabulary from 1.7 million websites.
- PhishBERT3_PredictURL.ipynb:  Fine-tunes the custom BERT-URL model and predicts phishing from URLs.

- PhishBertANN.ipynb: Combines the BERT predictions with an ANN that predicts phishing on URL-based features.
- PhishBertElectra.ipynb: Uses pre-trained BERT or ELECTRA models, performs fine-tuning, and predicts phishing from URLs.

- PhishResults.ipynb: Evaluates the results from all models and compares them against state-of-the-art approaches in the literature.
<br>



[1] H. Shirazi, S.R. Muramudalige, I. Ray, & A.P. Jayasumana, "Improved phishing detection algorithms using adversarial autoencoder synthesized data," 45th Conference on Local Computer Networks (LCN2020), Sydney, Australia, 2020. <br>


--Katherine Haynes <br>
--Updated: June 2021 <br>
--katherine.haynes@colostate.edu