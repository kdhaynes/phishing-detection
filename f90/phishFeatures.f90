program phishFeatures

  implicit none

  integer, parameter :: nFile=1730754
  character(len=128), parameter :: &
       fileIn = '../data/combo/common_crawls.txt', &
       fileOut = '../data/combo/legitFeatures.txt'
  integer, parameter :: printIndex=1730754
  
  !-----------------------------------------------------------
  !Variables
  character(len=1) charNow
  character(len=12) mA,mF,mP
  character(len=100) myFormat
  character(len=1800) :: urlIn, urlSub
  integer :: lensubdir, maxsubdir, minsubdir
  integer :: i, iascii, istop, uCount, &
       urllength, atsymbol, tildesymbol, &
       numdots, numdash, numunderscore, numpercent, &
       numampersand, numhash, numdollar, numquestion, &
       numequal, numbang, numsemicolon, numcolon, &
       numfslash, numbslash, &     
       numnumeric, numlower, numupper, numpunct, &
       lenpathlong, lenpathshort, endwithslash, &
       numdashinhost, numinhost, lenhost
  real :: percentnum, percentlower, percentcapital, percentpunct

  mA='(2a)'
  mF='(a,i6)'
  mP='(a,f10.4)'
  
  !-----------------------------------------------------------
  !Read and process the data

  open(unit=20,file=trim(fileIn))
  open(unit=30,file=trim(fileOut),status='unknown')
  write(30,'(6a)') 'urllength,atsymbol,tildesymbol,numdots,numdash,', &
       'numunderscore,numpercent,numampersand,numhash,numdollar,', &
       'numquestion,numequal,numbang,numsemicolon,numcolon,', &
       'numfslash,numbshash,numnumnumeric,numlower,numupper,numpunct,', &
       'percentnum,percentlower,percentcapital,percentpunct,maxsubdir,', &
       'minsubdir,endwithslash,lenhost,numdashinhost,numinhost'

  do uCount=1,nFile
     read(20,'(a2700)') urlIn

     urllength = len(trim(urlIn))
     atsymbol = index(urlIn,'@')
     if (atsymbol .ge. 1) atsymbol=1
     tildesymbol = index(urlIn,'~')
     if (tildesymbol .ge. 1) tildesymbol=1

     numdots = 0
     numdash = 0
     numunderscore = 0
     numpercent = 0
     numampersand = 0
     numhash = 0
     numdollar = 0
     numquestion = 0
     numequal = 0
     numbang = 0
     numsemicolon = 0
     numcolon = 0
     numfslash = 0
     numbslash = 0
     numnumeric = 0
     numlower = 0
     numupper = 0
     numpunct = 0
  
     do i=1,urllength
        charNow = urlIn(i:i)
        iascii = iachar(charNow)

        select case (charNow)
        case ('.')
           numdots = numdots + 1
        case ('-')
           numdash = numdash + 1
        case ('_')
           numunderscore = numunderscore + 1
        case ('%')
           numpercent = numpercent + 1
        case ('&')
           numampersand = numampersand + 1
        case ('#')
           numhash = numhash + 1
        case ('$')
           numdollar = numdollar + 1
        case ('?')
           numquestion = numquestion + 1
        case ('=')
           numequal = numequal + 1
        case ('!')
           numbang = numbang + 1
        case (';')
           numsemicolon = numsemicolon + 1
        case (':')
           numcolon = numcolon + 1
        case ('/')
           numfslash = numfslash + 1
        case ('\')
           numbslash = numbslash + 1
        end select

        if ((iascii .ge. 48) .and. (iascii .le. 57)) then
           numnumeric = numnumeric + 1
        else if ((iascii .ge. 65) .and. (iascii .le. 90)) then
           numupper = numupper + 1
        else if ((iascii .ge. 97) .and. (iascii .le. 122)) then
           numlower = numlower + 1
        else
           numpunct = numpunct + 1
        endif 
     enddo

     percentnum = numnumeric/float(urllength)
     percentlower = numlower/float(urllength)
     percentcapital = numupper/float(urllength)
     percentpunct = numpunct/float(urllength)

     !Find substructure info
     maxsubdir = 0
     minsubdir = 999.
     istop=0
     urlSub = urlIn
     do i=1,numfslash+1
        istop=index(urlSub,'/')
        if (istop .lt. 1) then
           istop=index(urlSub,' ')
        endif
        lensubdir=istop-1
        if (lensubdir .gt. maxsubdir) maxsubdir = lensubdir
        if ((lensubdir .lt. minsubdir) .and. (lensubdir .gt. 0)) minsubdir = lensubdir 
        urlSub=urlSub(istop+1:)
     enddo
     istop = index(urlIn,' ')
     endwithslash=0
     if (istop .gt. 1) then
        if (urlIn(istop-1:istop-1) .eq. '/') then
           endwithslash=1
        endif
     endif

     !Find domain info
     lenhost = index(urlIn,'/')-1
     if (lenhost .lt. 1) lenhost=urllength

     numdashinhost=0
     numinhost=0
  
     do i=1,lenhost
        charNow = urlIn(i:i)
        iascii = iachar(charNow)
        if (charNow .eq. '-') numdashinhost=numdashinhost+1
        if ((iascii .ge. 48) .and. (iascii .le. 57)) numinhost=numinhost+1
     enddo

     !Write Results to File
     myFormat='(i4.4,2(a1,i1),18(a1,i4.4),4(a1,f6.4),2(a1,i4.4),1(a1,i1),3(a1,i4.4))'
     write(30,myFormat) urllength, &
          ',',atsymbol,    &
          ',',tildesymbol, &
          ',',numdots,     &
          ',',numdash,     &
          ',',numunderscore, &
          ',',numpercent,    &
          ',',numampersand,  &
          ',',numhash,       &
          ',',numdollar,     &
          ',',numquestion,   &
          ',',numequal,      &
          ',',numbang,       &
          ',',numsemicolon,  &
          ',',numcolon,      &
          ',',numfslash,     &
          ',',numbslash,     &
          ',',numnumeric, &
          ',',numlower,      &
          ',',numupper,      &
          ',',numpunct,      &
          ',',percentnum,    &
          ',',percentlower,  &
          ',',percentcapital,  &
          ',',percentpunct,  &
          ',',maxsubdir,     &
          ',',minsubdir,     &
          ',',endwithslash,  &
          ',',lenhost,       &
          ',',numdashinhost,  &
          ',',numinhost
  
     !Print Results
     if (printIndex .eq. uCount) then
         print(mA),'URL       : ',trim(urlIn)
         print(mF),'URL Length: ',urllength
         print(mF),'@ Symbol  : ',atsymbol
         print(mF),'~ Symbol  : ',tildesymbol
         print(mF),'Number .  : ',numdots
         print(mF),'Number -  : ',numdash
         print(mF),'Number _  : ',numunderscore
         print(mF),'Number %  : ',numpercent
         print(mF),'Number &  : ',numampersand
         print(mF),'Number #  : ',numhash
         print(mF),'Number $  : ',numdollar
         print(mF),'Number ?  : ',numquestion
         print(mF),'Number =  : ',numequal
         print(mF),'Number !  : ',numbang
         print(mF),'Number ;  : ',numsemicolon
         print(mF),'Number :  : ',numcolon
         print(mF),'Number /  : ',numfslash
         print(mF),'Number \  : ',numbslash
         print(mF),'Num Numeric: ',numnumeric
         print(mF),'Num Lower  : ',numlower
         print(mF),'Num Upper  : ',numupper
         print(mF),'Num Punct  : ',numpunct
  
         print(mP),'% Numbers  : ',percentnum*100.
         print(mP),'% LowerCase: ',percentlower*100.
         print(mP),'% UpperCase: ',percentcapital*100.
         print(mP),'% Punctuate: ',percentpunct*100.

         print(mF),'Max Subdir : ',maxsubdir
         print(mF),'Min Subdir : ',minsubdir
         print(mF),'End With / : ',endwithslash

         print(mF),'Domain Len : ',lenhost
         print(mF),'Domain -   : ',numdashinhost
         print(mF),'Domain Num : ',numinhost
      
      endif

  enddo
  close(20)
  close(30)

end program
